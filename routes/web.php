<?php

Route::get('/{deviceId?}','MainController@showHomePage');
Route::get('live/','MainController@');
Route::get('/register-vehicle','MainController@registerVehicle');
Route::post('/register-vehicle','MainController@saveVehicleDetails');
Route::get('/devices','MainController@seeDevicesList');
Route::post('setData/{deviceId}','MainController@setPopulateData');
Route::post('setTelemeticsData','MainController@setTelemeticsData');
Route::get('telematics/{deviceId?}', 'MainController@showTelematics');