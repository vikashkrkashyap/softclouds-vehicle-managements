<?php

/**
 * @author :=> Vikash <vikashkrkashyap@gmail.com>
 * @since :=> 5 september 2017
 */
namespace App\Http\Controllers;

use App\Models\DeviceConfig;
use App\Models\DeviceDetails;
use App\Models\DeviceTelematics;
use App\Models\ShowData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MainController extends Controller
{
    public function showHomePage($deviceId = null)
    {
        if($deviceId) {
            return $this->showIndex($deviceId);
        }

        $firstDevice = $this->getFirstDevice();
        if($firstDevice) {
            return $this->showIndex($firstDevice->device_id);
        }
        else{
            return response()->json([
                'code' => 404,
                'message' => 'No device found'
            ]);
        }
    }

    public function registerVehicle()
    {
        $vehicle = DeviceConfig::orderBy('vehicle_id', 'desc')->first();
        $vehicleId = $vehicle ? $vehicle->vehicle_id : 0;
        $prefix = 'VECH';
        $prevNo = (int)substr($vehicleId, strlen($prefix));
        $vehicleId = $this->getUniqueIds('VECH', $prevNo);

        return view('device_registration', compact('vehicleId'));
    }

    public function saveVehicleDetails(Request $request)
    {
        $rules  = [
            'vehicle_id' => 'required',
            'registration_id' => 'required|min:3',
            'protocol' => 'required',
            'entry_date' => 'required|date_format:"d F, Y"'
        ];

        $message = [
            'registration_id.required' => 'Registration number is required',
            'registration_id.min' => 'Registration number cannot be less than 3 characters',
            'entry_date.date_format' => 'Entry date should be in the format "d F, Y", Ex: 20 September, 2017'
        ];

        $validator = validator($request->all(), $rules, $message);
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $vehicle = new DeviceConfig();
        $vehicle->vehicle_id = $request->input('vehicle_id');
        $vehicle->registration_id = $request->input('registration_id');
        $vehicle->protocol = $request->input('protocol');
        $vehicle->entry_date = strtotime($request->input('entry_date'));
        $vehicle->save();

        return "Vehicle registered successfully";
    }

    public function seeDevicesList()
    {
        $devices = DeviceDetails::orderBy('device_id', 'asc')->get();

        return view('device', compact('devices'));
    }

    public function setPopulateData($deviceId, Request $request){

        $response = ['success' => false];
        $payload = $request->input('payload');

        $error = $this->validateDevice($deviceId);
        if($error){
            $response['error'] = "Device Doesn't exist!";
            return response()->json($response);
        }

        try{
            $data = $this->getDeviceData($deviceId, $payload);
            $hasDataExist = ShowData::where('device_id', $deviceId)->first();

            if($hasDataExist){
                $hasDataExist->update($data);
            }
            else{
               ShowData::create($data);
            }
        }catch (\Exception $e){

            return response()->json(['success' => false, 'error' => 'Server error in updating device data']);
        }

        return response()->json(['success' => true, 'payload' => $data]);
    }

    public function showTelematics($deviceId = null)
    {
        if(is_null($deviceId)){
            $deviceId = $this->getFirstDevice()->device_id;
        }

        $deviceTelemetics  = DeviceTelematics::where('device_id', $deviceId)->first();
        if(!$deviceTelemetics){
            $deviceTelemetics = $this->initDeviceTelemetics($deviceId);
        }

        if($deviceTelemetics) {
            return view('telematics', compact('deviceTelemetics'));
        }
        else {
            return response()->json([
                'code' => 404,
                'message' => 'No device found'
            ]);
        }
    }

    protected function setTelemeticsData(Request $request)
    {
        $payload = $request->input('payload');
        $deviceId = array_get($payload,'VIN',false);

        try{
            if(!$deviceId) {
                $deviceId = $this->getFirstDevice()->device_id;
            }

            $data = $this->getTelematicsData($payload, $deviceId);

            $deviceTelemetics = DeviceTelematics::where('device_id', $deviceId)->first();

            if($deviceTelemetics)
            {
                $deviceTelemetics->update($data);
                return $this->responseData(true, $data);

            }
            else
            {
                DeviceTelematics::create($data);
                return $this->responseData(true, $data);
            }
        }
        catch (\Exception $e) {
            Log::info('Telmetics data saving error  ::=> '.$e->getMessage());

            return response()->json([
                'success' => false,
                'error' => 'Some error occurred while updating the data'
            ]);
        }

    }

    private function getTelematicsData($payload, $deviceId)
    {
        $data = [
            'device_id' => $deviceId,
            'left_front_light' => array_get($payload,'leftFrontLight',0) == 'true' ? 1 : 0,
            'right_front_light' => array_get($payload,'rightFrontLight',0) == 'true' ? 1 : 0,
            'left_front_airbag' => array_get($payload,'leftFrontAirbag', 0) == 'true' ? 1 : 0,
            'right_front_airbag' => array_get($payload,'rightFrontAirbag',0) == 'true' ? 1 : 0,
            'left_back_airbag' => array_get($payload, 'leftBackAirbag',false) == "true" ? 1: 0,
            'right_back_airbag' => array_get($payload, 'rightBackAirBag', 0) == "true" ? 1 :0,
            'left_back_light' => array_get($payload,'leftBackLight',0) == "true" ? 1 : 0,
            'right_back_light' => array_get($payload,'rightBackLight',0) == 'true' ? 1 : 0,
            'hand_brake' => array_get($payload,'handBrake',0) == "true" ? 1 : 0,
            'left_front_tier' => array_get($payload,'leftFrontTire',0),
            'right_front_tier' => array_get($payload,'rightFrontTire',0),
            'left_back_tire' => array_get($payload,'leftBackTire',0),
            'right_back_tire' => array_get($payload,'rightBackTire',0),
            'odometer' => array_get($payload,'odometer',0),
            'acceleration' => array_get($payload,'Acceleration',0),
            'rpm' => array_get($payload,'rpm',0),
            'speed' => array_get($payload,'speed',0),
            'wiper_fluid' => array_get($payload,'wiperFluid',0),
            'oil_level' => array_get($payload,'oilLevel',0)
        ];

        return $data;
    }

    private function validateDevice($deviceId){

        $device = DeviceDetails::find($deviceId);
        if(!$device){
            return "Invalid device id";
        }
        else {
            return false;
        }
    }

//    private function broadcastData($data)
//    {
//
//        $options = array(
//            'cluster' => 'ap2',
//            'encrypted' => true
//        );
//        $pusher = new Pusher(
//            'e9282422f2a20f13c198',
//            'b5b4752b64428497bd45',
//            '400257',
//            $options
//        );
//
//        $pusher->trigger('my-channel' . $data['device_id'], 'my-event', ['message'=>'hello']);
//    }

    private function getDeviceData($deviceId, $payload){

        $data = [
            'engine_load' => array_get($payload,'ENGINE_LOAD',0),
            'engine_coolent_temp' => array_get($payload,'ENGINE_COOLANT_TEMP',0),
            'fuel_pressure' => array_get($payload,'FUEL_PRESSURE',0),
            'intake_manifold_pressure' => array_get($payload,'INTAKE_MANIFOLD_PRESSURE', 0),
            'engine_rpm' => array_get($payload,'ENGINE_RPM',0),
            'speed' => array_get($payload, 'SPEED',0),
            'intake_air_temp' => array_get($payload, 'AIR_INTAKE_TEMP', 0),
            'throttle_position' => array_get($payload,'THROTTLE_POS',0),
            'dist_travelled_with_MIL' => array_get($payload,'DISTANCE_TRAVELED_MIL_ON',0),
            'fuel_rail_pressure' => array_get($payload,'FUEL_RAIL_PRESSURE',0),
            'barometric_pressure' => array_get($payload,'BAROMETRIC_PRESSURE',0),
            'fuel_level' => array_get($payload,'FUEL_LEVEL',0),
            'ambient_air_temp' => array_get($payload,'AMBIENT_AIR_TEMP',0),
            'engine_oil_temp' => array_get($payload,'ENGINE_OIL_TEMP',0)
        ];

        $data = array_map(function($entry){
            return (float)$entry;
        }, $data);

        $data['device_id'] =   $deviceId;
        $data['runtime_since_engine_start'] = array_get($payload,'ENGINE_RUNTIME',0);
        $data['lat'] = array_get($payload,'lat',0);
        $data['long'] = array_get($payload,'long',0);

        return $data;
    }

    private function showIndex($deviceId)
    {
        $error = $this->validateDevice($deviceId);
        if($error) {
            return $error;
        }

        $data = ShowData::where('device_id', $deviceId)->first();
        if(!$data){
            $data = $this->initShowData($deviceId);
        }

        return view('index', compact('data'));
    }


    private function initShowData($deviceId){
        return ShowData::create([
            'device_id' => $deviceId
        ]);
    }

    private function initDeviceTelemetics($deviceId)
    {
        return DeviceTelematics::create([
            'device_id' => $deviceId
        ]);
    }

    private function getFirstDevice()
    {
        return DeviceDetails::orderBy('device_id','ASC')->first();
    }

    private function responseData($success, $payload)
    {
        $response = [
            'success' => $success
        ];

        if($payload){
            $response['payload'] = $payload;
        }

        return response()->json($response);
    }
}
