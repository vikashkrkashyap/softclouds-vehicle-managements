<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelematicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telematics', function(Blueprint $table){

            $table->increments('id');
            $table->string('device_id')->nullable()->references('device_id')->on('device_details');
            $table->boolean('left_front_light')->nullable();
            $table->boolean('right_front_light')->nullable();
            $table->boolean('left_front_airbag')->nullable();
            $table->boolean('right_front_airbag')->nullable();
            $table->boolean('left_back_airbag')->nullable();
            $table->boolean('right_back_airbag')->nullable();
            $table->boolean('left_back_light')->nullable();
            $table->boolean('right_back_light')->nullable();
            $table->boolean('hand_brake')->nullable();
            $table->integer('left_front_tier')->nullable();
            $table->integer('right_front_tier')->nullable();
            $table->integer('left_back_tire')->nullable();
            $table->integer('right_back_tire')->nullable();
            $table->integer('odometer')->nullable();
            $table->integer('acceleration')->nullable();
            $table->integer('rpm')->nullable();
            $table->integer('speed')->nullable();
            $table->integer('wiper_fluid')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('oil_level')->nullable();
            $table->string('max_value')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telematics');
    }
}
