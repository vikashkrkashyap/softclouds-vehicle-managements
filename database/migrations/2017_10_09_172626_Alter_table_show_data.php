<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableShowData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('show_data', function(Blueprint $table){

            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->float('engine_oil_temp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('show_data', function(Blueprint $table){

            $table->dropColumn('lat');
            $table->dropColumn('long');
            $table->dropColumn('engine_oil_temp');
        });
    }
}
