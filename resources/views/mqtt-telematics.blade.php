<script src="{{ url('js/mqtt-paho.js') }}" type="text/javascript"></script>
<script>

    // Create a client instance
    var client = new Paho.MQTT.Client('52.14.165.48', 8080, 'AndroidMqttClientOBD');
    var topic = "softclouds/semantics/#";


    client.onMessageArrived = function (message) {

        var payload = getPayloadAsJSON(message.payloadString);

        $.ajax({
            type : 'POST',
            dataType:'JSON',
            url : "{{ url('setTelemeticsData') }}",
            data : {"_token": "{{ csrf_token() }}", "payload" : payload},
            success : function(data){
                if(data.success && data.payload){
                    var responseData = data.payload;
                    responseData.left_front_light  === 1 ? $('#leftFrontLight').attr('checked', true) : $('#leftFrontLight').attr('checked', false);
                    responseData.right_front_light  === 1 ? $('#rightFrontLight').attr('checked', true) : $('#rightFrontLight').attr('checked', false);
                    responseData.left_front_airbag  === 1 ? $('#leftFrontAirbag').attr('checked', true) : $('#leftFrontAirbag').attr('checked', false);
                    responseData.left_back_airbag  === 1 ? $('#leftBackAirbag').attr('checked', true) : $('#leftBackAirbag').attr('checked', false);
                    responseData.right_front_airbag  === 1 ? $('#rightFrontAirbag').attr('checked', true) : $('#rightFrontAirbag').attr('checked', false);
                    responseData.right_back_airbag  === 1 ? $('#rightBackAirbag').attr('checked', true) : $('#rightBackAirbag').attr('checked', false);
                    responseData.left_back_light  === 1 ? $('#leftBackLight').attr('checked', true) : $('#leftBackLight').attr('checked', false);
                    responseData.right_back_light  === 1 ? $('#rightBackLight').attr('checked', true) : $('#rightBackLight').attr('checked', false);
                    responseData.hand_brake  === 1 ? $('#handBrake').attr('checked', true) : $('#handBrake').attr('checked', false);
                    responseData.left_front_tier ? $('#leftFrontTire').text(responseData.left_front_tier) : null;
                    responseData.right_front_tier ? $('#rightFrontTire').text(responseData.right_front_tier) : null;
                    responseData.left_back_tire ? $('#leftBackTire').text(responseData.left_back_tire) : null;
                    responseData.right_back_tier ? $('#rightBackTire').text(responseData.right_back_tier) : null;
                    responseData.odometer ? $('#odometer').text(responseData.odometer) : null;
                    responseData.acceleration ? $('#acceleration').text(responseData.acceleration) : null;
                    responseData.rpm ? $('#rpm').text(responseData.rpm) : null;
                    responseData.speed ? $('#speed').text(responseData.speed) : null;
                    responseData.wiper_fluid ? $('#wiperFluid').text(responseData.wiper_fluid) : null;
                    responseData.oil_level ? $('#oilLevel').text(responseData.oil_level) : null;
                }
                else{
                    console.error(data.error)
                }
            },
            fail: function(){
                console.log('Failed to show data');
            }
        });
    };

    // connect the client
    client.connect({
        onSuccess: onConnect,
        userName : 'softclouds',
        password : 'pass4softclouds'
    });

    // called when the client connects
    function onConnect() {
        // Once a connection has been made, make a subscription and send a message.
        client.subscribe(topic);
        console.log('connection established');
       // sM();
    }

    function sM() {
        message = new Paho.MQTT.Message('ok');
        message.destinationName = "softclouds/semantics/#";
        client.publish(message);
        console.log('message sent successfully');
    }

    client.onConnectionLost = function (responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("onConnectionLost:" + responseObject.errorMessage);
        }
    };

    function getPayloadAsJSON(payload) {
        var payload_array = payload.split(",");
        var return_json = {};
        payload_array.forEach(function (kv) {
            var kv_array = (kv.indexOf("=") !== -1 ?  kv.split("=") : kv.split(":"));
            var k = kv_array[0];
            var v = kv_array[1];
            return_json[k.trim()] = v.trim();
        });

        return return_json;
    }



</script>