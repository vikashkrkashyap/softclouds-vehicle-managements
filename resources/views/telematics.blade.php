<html>
<head>
    <title>Vehicle Management App | Telematics</title>

    <!-- material Css CDN -->

    <link rel="stylesheet" href="{{ url('css/materialize.min.css') }}">

    <script type="text/javascript" src="{{ url('js/jquery-2.2.3.js') }}"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="{{ url('js/materialize.min.js') }}"></script>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="{{ url('css/telematics.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">

</head>

<body class="content_center" style="background:#000">
<div class="backgroud_transparent"></div>
<div class="background_page"></div>
<!-- Container Block -->


<div class = "container container_property">
    <div class = "row action_bar">
        <div class="title">
            <img src="{{ url('images/softcloud_social_share.png') }}" width="160px">
        </div>
    </div>

    <div class="form_content">

        <div class="heading_form">
            <h5>Telematics</h5>
        </div>

        <div class="border_form_content">
            <div class="row header-row">
                <div class="col s3">
                    <img class="icons" src="{{ url('images/carlight_on.png') }}">
                    <p>Left Front Light</p>
                    <label class="switch">
                        <input type="checkbox" id="leftFrontLight" @if($deviceTelemetics->left_front_light) checked @endif>
                        <span class="slider round"></span>
                    </label>
                </div>

                <div class="col s3">
                    <img class="icons" src="{{ url('images/tire.png') }}">
                    <p>Left Front Tire</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value"  id="leftFrontTire">{{ $deviceTelemetics->left_front_tier ? $deviceTelemetics->left_front_tier : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s3">
                    <img class="icons" src="{{ url('images/tire.png') }}">
                    <p>Right Front Tire</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value" id="rightFrontTire">{{ $deviceTelemetics->right_front_tier ? $deviceTelemetics->right_front_tier : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s3">
                    <img class="icons" src="{{ url('images/carlight_on.png') }}">
                    <p>Right Front Light</p>
                    <label class="switch">
                        <input type="checkbox" id="rightFrontLight" @if($deviceTelemetics->right_front_light) checked @endif>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            <div class="row middle-row">
                <div class="col s4">
                    <div class="row">
                        <img class="icons" src="{{ url('images/airbag.png') }}">
                        <p>Left Front Airbag</p>
                        <label class="switch">
                            <input type="checkbox" id="leftFrontAirbag" @if($deviceTelemetics->left_front_airbag) checked @endif>
                            <span class="slider round" id="redSwitch"></span>
                        </label>
                    </div>
                    <div class="row">
                        <img class="icons" src="{{ url('images/airbag.png') }}">
                        <p>Left Back Airbag</p>
                        <label class="switch">
                            <input type="checkbox" id="leftBackAirbag" @if($deviceTelemetics->left_back_airbag) checked @endif>
                            <span class="slider round" id="redSwitch"></span>
                        </label>
                    </div>
                </div>
                <div class="col s4">
                    <img height="300px" src="{{ url('images/model_car.png') }}">
                </div>
                <div class="col s4">
                    <div class="row">
                        <img class="icons" src="{{ url('images/airbag.png') }}">
                        <p>Right Front Airbag</p>
                        <label class="switch">
                            <input type="checkbox" id="rightFrontAirbag" @if($deviceTelemetics->right_front_airbag) checked @endif>
                            <span class="slider round" id="redSwitch"></span>
                        </label>
                    </div>
                    <div class="row">
                        <img class="icons" src="{{ url('images/airbag.png') }}">
                        <p>Right Back Airbag</p>
                        <label class="switch">
                            <input type="checkbox" id="rightBackAirbag" @if($deviceTelemetics->right_back_airbag) checked @endif>
                            <span class="slider round" id="redSwitch"></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row footer-row">
                <div class="col s3">
                    <img class="icons" src="{{ url('images/back_light_on.png') }}">
                    <p>Left Back Light</p>
                    <label class="switch">
                        <input type="checkbox" id="leftBackLight" @if($deviceTelemetics->left_back_light) checked @endif>
                        <span class="slider round"></span>
                    </label>
                </div>

                <div class="col s3">
                    <img class="icons" src="{{ url('images/tire.png') }}">
                    <p>Left Back Tire</p>
                    <p class="data-value" ><span class="decrement">-</span><span class="result-value" id="leftBackTire">{{ $deviceTelemetics->left_back_tire ? $deviceTelemetics->left_back_tire : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s3">
                    <img class="icons" src="{{ url('images/tire.png') }}">
                    <p>Right Back Tire</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value"  id="rightBackTire">{{ $deviceTelemetics->left_back_tire ? $deviceTelemetics->left_back_tire : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s3">
                    <img class="icons" src="{{ url('images/back_light_on.png') }}">
                    <p>Right Back Light</p>
                    <label class="switch">
                        <input type="checkbox" id="rightBackLight" @if($deviceTelemetics->right_back_light) checked @endif>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            <div class="row footer-row">
                <div class="col s2">
                    <p>Odometer</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value" id="odometer">{{ $deviceTelemetics->odometer ? $deviceTelemetics->odometer : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s2">
                    <p>Acceleration</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value" id="acceleration">{{ $deviceTelemetics->acceleration ? $deviceTelemetics->acceleration : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s2">
                    <p>RPM</p>
                    <p class="data-value" ><span class="decrement">-</span><span class="result-value" id="rpm">{{ $deviceTelemetics->rpm ? $deviceTelemetics->rpm : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s2">
                    <p>Speed</p>
                    <p class="data-value" ><span class="decrement">-</span><span class="result-value" id="speed">{{ $deviceTelemetics->speed ? $deviceTelemetics->speed : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s2">
                    <p>Oil Level</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value" id="oilLevel">{{ $deviceTelemetics->oil_level ? $deviceTelemetics->oil_level : 0 }}</span><span class="increment">+</span></p>
                </div>

                <div class="col s2">
                    <p>Wiper Fluid</p>
                    <p class="data-value"><span class="decrement">-</span><span class="result-value"  id="wiperFluid">{{ $deviceTelemetics->wiper_fluid ? $deviceTelemetics->wiper_fluid : 0 }}</span><span class="increment">+</span></p>
                </div>
            </div>

            <div class="row footer-row">
                <div class="col s3">
                    <p>Hand Brake</p>
                    <label class="switch">
                        <input type="checkbox" id="handBrake" @if($deviceTelemetics->left_front_airbag) checked @endif>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">

    $('.increment').click(function(event){
        event.preventDefault();

        var currentTarget = $(this).parent('.data-value').find('.result-value');
        var currentValue = parseInt(currentTarget.text());

        // set the increment value
        currentTarget.text(currentValue+1);
    });

    $('.decrement').click(function(event){
        event.preventDefault();

        var currentTarget = $(this).parent('.data-value').find('.result-value');
        var currentValue = parseInt(currentTarget.text());

        if(currentValue === 0) {
            alert('Value cannot be negative');
            return false;
        }

        // set the decrement value
        currentTarget.text(currentValue -1);
    });

</script>
</body>
@include('mqtt-telematics');
</html>
